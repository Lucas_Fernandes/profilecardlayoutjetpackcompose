package com.repository.profilecardlayoutjetpackcompose

data class UserProfile(val id: Int, val name: String, val status: Boolean, val pictureUrl: String)